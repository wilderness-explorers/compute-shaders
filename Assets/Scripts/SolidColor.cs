using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidColor : MonoBehaviour
{
    public ComputeShader shader;
    public int textureResolution = 256;

    public string kernelName = "SolidRed";

    private Renderer rend;
    private RenderTexture outputTexture;
    private int kernelHandle;

    void Start()
    {
        //Create a render texture. Size: width, height, depth - defined by resolution.
        //No depth buffer in this. Can be 16 or 24 if stencil setting is to be used.
        outputTexture = new RenderTexture(textureResolution, textureResolution, 0);

        //To allow the compute shader to write to the texture.
        outputTexture.enableRandomWrite = true;

        //After initializing instance of render texture, to make it available:
        outputTexture.Create();

        //Renderer to update Quad's texture to use the custom one.
        rend = GetComponent<Renderer>();
        rend.enabled = true;

        InitializeShader();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            DispatchShader(textureResolution / 8, textureResolution / 8);
        }
    }

    private void InitializeShader()
    {
        kernelHandle = shader.FindKernel(kernelName);
        shader.SetInt("texResolution", textureResolution);

        // Create a Vector4 with parameters x, y, width, height.
        // Pass this to the shader using SetVector.
        int halfRes = textureResolution >> 1;
        int quarterRes = textureResolution >> 2;
        Vector4 rect = new Vector4(quarterRes, quarterRes, halfRes, halfRes);

        // For rect vector: (x, y) = bottom left corner; z = width; w = height;
        shader.SetVector("rect", rect);

        shader.SetTexture(kernelHandle, "Result", outputTexture);

        rend.material.SetTexture("_MainTex", outputTexture);

        // Normally (texResolution/num of threads) blocks is reqd in x and y directions. In this case numthreads(8,8,1).
        // Following gives only half the number of blocks reqd to fill the pixels in x and y directions.
        DispatchShader(textureResolution / 16, textureResolution / 16);
    }

    private void DispatchShader(int x, int y)
    {
        //A kernel can be called from Monobehaviour using compute shader method - Dispatch().
        shader.Dispatch(kernelHandle, x, y, 1);
    }
}
